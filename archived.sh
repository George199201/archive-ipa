#! /bin/sh
# 需要将该脚本放在xcdeproj的同级目录下执行

### 需要修改的内容 ###
# 项目名称
project_name=$(find . -name *.xcodeproj | awk -F "[/.]" '{print $(NF-1)}')
project_name="archived"
# 方案名称
scheme_name="archived"
# 项目类型，如果项目是cocoapod，就改为xcworkspace，否则用xocdeproj
project_type="xcodeproj"

# 当前脚本执行目录
script_path="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
# 工作空间目录
workspace_path="${script_path}/${project_name}.xcworkspace"
# 项目目录
project_path="${script_path}/${project_name}.xcodeproj"
# Info.plist路径
info_plist="${script_path}/${project_name}/Info.plist"
# 打包模式，Debug/Release
configuration="Release"
# 编译生成的.xcarchive保存路径
archive_path="${script_path}/archive/${project_name}.xcarchive"
# ipa生成的路径
ipa_path="${script_path}/archive/ipa"
# 导出项目所需要的plist文件，该文件可以手动编译一次会自动生成
export_plist_path="${script_path}/ExportOptions.plist"

# # 脚本目录的上一级目录
# project_path=$(dirname "$scrip_path")
# echo $project_path

function updateVersion() {
    # 获取版本信息
    version=$(/usr/libexec/PlistBuddy -c "Print CFBundleShortVersionString" $info_plist)
    build=$(/usr/libexec/PlistBuddy -c "Print CFBundleVersion" $info_plist)

    # 这里取出version的第三个值
    thirdPartVersonNum=$(echo $version | awk -F "." '{print $3}')
    thirdPartVersonNum=$(($thirdPartVersonNum + 1))
    version=$(echo $version | awk -F "." '{print $1 "." $2 ".'$thirdPartVersonNum'" }')

    # 更新build
    build=$(expr $build + 1)

    echo $version
    echo $build
    echo $bundleID

    # 设置新的版本信息
    /usr/libexec/PlistBuddy -c "Set :CFBundleShortVersionString $version" "${info_plist}"
    # 设置新的build信息
    /usr/libexec/PlistBuddy -c "Set :CFBundleVersion $build" ${info_plist}
}

# updateVersion;

function cleanProject() {
    xcodebuild \
        clean -configuration ${configuration} -quiet || exit
}

# 使用Project打包
function archiveByProject() {
    xcodebuild archive \
        -project ${project_path} \
        -scheme "${scheme_name}" \
        -configuration ${configuration} \
        -archivePath ${archive_path} -quiet || exit
}

# 使用workspace打包
function archiveByWorkspace() {
    xcodebuild archive \
        -workspace ${workspace_path} \
        -scheme "${scheme_name}" \
        -configuration ${configuration} \
        -archivePath ${archive_path} -quiet || exit
}

# 导出archive为ipa
function exportArchive() {
    xcodebuild -exportArchive -archivePath ${archive_path} \
        -configuration ${configuration} \
        -exportPath ${ipa_path} \
        -exportOptionsPlist ${export_plist_path} \
        -quiet || exit
}

#archive
if [ -d $archive_path ]; then
    echo "已经archive"
else
    if [ $project_type == "xcodeproj" ]; then
        archiveByProject
    else
        archiveByWorkspace
    fi
fi

#ipa
if [ -d $ipa_path ]; then
    echo "已经ipa"
else
    exportArchive
fi

#upload
function uploadPGY() {
    curl -F "file=@${ipa_path}/${scheme_name}.ipa" -F "uKey=fee85d13ecc10a5ec9ada3970980effe" -F "_api_key=6020ead819ab9a24d0ad6a39b82d50e1" -F "password=maxueshan123" https://www.pgyer.com/apiv1/app/upload
}
function uploadAppStore() {
    #验证并上传到App Store
    # 将-u 后面的XXX替换成自己的AppleID的账号，-p后面的XXX替换成自己的密码
    altoolPath="/Applications/Xcode.app/Contents/Applications/Application Loader.app/Contents/Frameworks/ITunesSoftwareService.framework/Versions/A/Support/altool"
    "$altoolPath" --validate-app -f ${ipa_path}/${scheme_name}.ipa -u XXX -p XXX -t ios --output-format xml
    "$altoolPath" --upload-app -f ${ipa_path}/${scheme_name}.ipa -u XXX -p XXX -t ios --output-format xml
}
function uploadFir() {
    #上传到Fir
    # 将XXX替换成自己的Fir平台的token
    fir login -T XXX
    fir publish ${ipa_path}/${scheme_name}.ipa
}

if [ $? = 0 ]; then
    echo 'success'
else
    echo 'failure'
fi
