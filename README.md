# adhoc

#### 介绍
搭建一个通过内外网页安装ipa的服务器



#### 使用说明

1. archived.sh是自动打包编译的shell脚本

2. installer.sh 是自动安装mobileprovision和p12文件的shell脚本，需要一个证书所在的目录作为参数

   ```powershell
   installer.sh /Users/admin/Desktop/zs
   ```





#### 参与贡献

1.  虞嘉伟
